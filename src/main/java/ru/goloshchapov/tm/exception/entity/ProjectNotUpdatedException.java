package ru.goloshchapov.tm.exception.entity;

import ru.goloshchapov.tm.exception.AbstractException;

public class ProjectNotUpdatedException extends AbstractException {

    public ProjectNotUpdatedException() {
        super("Error! Project not updated...");
    }

}
