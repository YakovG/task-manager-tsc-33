package ru.goloshchapov.tm.exception.system;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(@Nullable final String value) {
        super("Error! Command " + value + " is not found...");
    }

}
