package ru.goloshchapov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.endpoint.*;

public interface EndpointLocator {

    @NotNull CalculatorEndpoint getCalculatorEndpoint();

    @NotNull ProjectEndpoint getProjectEndpoint();

    @NotNull SessionEndpoint getSessionEndpoint();

    @NotNull TaskProjectEndpoint getTaskProjectEndpoint();

    @NotNull TaskEndpoint getTaskEndpoint();

    @NotNull UserEndpoint getUserEndpoint();

}
