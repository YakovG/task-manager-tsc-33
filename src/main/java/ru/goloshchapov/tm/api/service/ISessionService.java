package ru.goloshchapov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IService;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.Session;

import java.util.List;

public interface ISessionService extends IService<Session> {

    List<Session> getListSession(Session session);

    @Nullable Session findSessionByLogin(@Nullable String login);

    void closeSessionByLogin(@Nullable String login, @Nullable String password);

    @Nullable Session sign(@Nullable Session session);

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    @Nullable Session open(@Nullable String login, @Nullable String password);

    void close(@Nullable Session session);

    boolean isValid(@Nullable Session session);

    void validate (@Nullable Session session);

    void validate(@Nullable Session session, @Nullable Role role);
}
