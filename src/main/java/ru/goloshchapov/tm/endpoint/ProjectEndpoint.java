package ru.goloshchapov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public final class ProjectEndpoint extends AbstractEndpoint {

    private final ServiceLocator serviceLocator;

    public ProjectEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @SneakyThrows
    public @Nullable List<Project> findProjectAll(
            @WebParam (name = "session") final Session session) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    public void addProjectAll(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "collection") final Collection<Project> collection
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().addAll(session.getUserId(), collection);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project addProject(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "project") final Project model
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().add(session.getUserId(), model);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public List<Project> findAllProjectByUserId(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAllByUserId(session.getUserId());
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project findProjectById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "projectId") final String modelId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneById(session.getUserId(), modelId);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project findProjectByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project findProjectByNameAndUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByName(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    public int sizeProject(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().size(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    public void removeProject(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final Project model
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(session.getUserId(), model);
    }

    @WebMethod
    @SneakyThrows
    public void clearProject(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project removeProjectById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "projectId") final String modelId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneById(session.getUserId(), modelId);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project removeProjectByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project removeProjectByName(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project startProjectById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "projectId") final String modelId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startOneById(session.getUserId(), modelId);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project startProjectByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project startProjectByName(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startOneByName(session.getUserId(), name);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project finishProjectById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "projectId") final String modelId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishOneById(session.getUserId(), modelId);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project finishProjectByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project finishProjectByName(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishOneByName(session.getUserId(), name);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public List<Project> sortedProjectBy(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "sortCheck") final String sortCheck
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().sortedBy(session.getUserId(), sortCheck);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project updateProjectById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "projectId") final String modelId,
            @WebParam (name = "name") final String name,
            @WebParam (name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateOneById(session.getUserId(), modelId, name, description);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project updateProjectByIndex(@WebParam (name = "session") final Session session,
                                        @WebParam (name = "index") final Integer index,
                                        @WebParam (name = "name") final String name,
                                        @WebParam (name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateOneByIndex(session.getUserId(), index, name, description);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project changeProjectStatusById(@WebParam (name = "session") final Session session,
                                           @WebParam (name = "projectId") String id,
                                           @WebParam (name = "statusChange") String statusChange
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeOneStatusById(session.getUserId(), id, statusChange);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project changeProjectStatusByName(@WebParam (name = "session") final Session session,
                                             @WebParam (name = "name") final String name,
                                             @WebParam (name = "statusChange") final String statusChange
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeOneStatusByName(session.getUserId(), name, statusChange);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project changeProjectStatusByIndex(@WebParam (name = "session") final Session session,
                                              @WebParam (name = "index") final int index,
                                              @WebParam (name = "statusChange") String statusChange
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeOneStatusByIndex(session.getUserId(), index, statusChange);
    }

    @WebMethod
    @SneakyThrows
    @NotNull
    public Project addProjectByName(@WebParam (name = "session") final Session session,
                                    @WebParam (name = "name") final String name,
                                    @WebParam (name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().add(session.getUserId(), name, description);
    }
}
