package ru.goloshchapov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.model.Session;
import ru.goloshchapov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public final class TaskEndpoint extends AbstractEndpoint{

    private final ServiceLocator serviceLocator;

    public TaskEndpoint (ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @SneakyThrows
    public void addTaskAll(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "collection") Collection<Task> collection
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().addAll(session.getUserId(), collection);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task addTask(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "task") final Task model
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().add(session.getUserId(), model);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable List<Task> findTaskAll(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    public @Nullable List<Task> findTaskAllByUserId(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAllByUserId(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task findTaskById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "taskId") final String modelId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneById(session.getUserId(), modelId);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task findTaskByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task findTaskByName(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByName(session.getUserId(), name);
    }


    @WebMethod
    @SneakyThrows
    public int sizeTask(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().size(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    public void removeTask(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "task") final Task model
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(session.getUserId(), model);
    }

    @WebMethod
    @SneakyThrows
    public void clearTask(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task removeTaskById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "taskId") final String modelId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneById(session.getUserId(), modelId);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task removeTaskByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task removeTaskByName(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task startTaskById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "taskId") final String modelId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startOneById(session.getUserId(), modelId);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task startTaskByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task startTaskByName(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startOneByName(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task finishTaskById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "taskId") final String modelId
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishOneById(session.getUserId(), modelId);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task finishTaskByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task finishTaskByName(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishOneByName(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable List<Task> sortedTaskBy(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "sortCheck") final String sortCheck) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().sortedBy(session.getUserId(), sortCheck);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task updateTaskById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "taskId") final String modelId,
            @WebParam (name = "name") final String name,
            @WebParam (name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateOneById(session.getUserId(), modelId, name, description);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task updateTaskByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index,
            @WebParam (name = "name") String name,
            @WebParam (name = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateOneByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task changeTaskStatusById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "taskId") final String id,
            @WebParam (name = "statusChange") String statusChange
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeOneStatusById(session.getUserId(), id, statusChange);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task changeTaskStatusByName(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name,
            @WebParam (name = "statusChange") String statusChange
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeOneStatusByName(session.getUserId(), name, statusChange);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task changeTaskStatusByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final int index,
            @WebParam (name = "statusChange") String statusChange
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeOneStatusByIndex(session.getUserId(), index, statusChange);
    }

}
