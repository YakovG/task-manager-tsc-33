package ru.goloshchapov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.service.ServiceLocator;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Session;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public final class AdminEndpoint extends AbstractEndpoint{

    private final ServiceLocator serviceLocator;

    public AdminEndpoint(ServiceLocator serviceLocator) {this.serviceLocator = serviceLocator;}

    @WebMethod
    @SneakyThrows
    public void addUserAll(
            @WebParam(name = "session") final Session session,
            @WebParam (name = "collection") final Collection<User> collection
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().addAll(collection);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable User addUser(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "user") final User entity
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().add(entity);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable List<User> findUserAll(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findAll();
    }

    @WebMethod
    @SneakyThrows
    public @Nullable User findUserById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "userId") final String id) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findOneById(id);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable User findUserByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findOneByIndex(index);
    }

    @WebMethod
    @SneakyThrows
    public boolean isUserAbsentById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "userId") final String id) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().isAbsentById(id);
    }

    @WebMethod
    @SneakyThrows
    public boolean isUserAbsentByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().isAbsentByIndex(index);
    }

    @WebMethod
    @SneakyThrows
    public String getUserIdByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().getIdByIndex(index);
    }

    @WebMethod
    @SneakyThrows
    public int sizeUser(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().size();
    }

    @WebMethod
    @SneakyThrows
    public void clearUser(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().clear();
    }

    @WebMethod
    @SneakyThrows
    public @Nullable User removeUserById(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "userId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeOneById(id);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable User removeUserByIndex(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeOneByIndex(index);
    }

    @WebMethod
    @SneakyThrows
    public boolean isUserLoginExists(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "login") final String login) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().isLoginExists(login);
    }

    @WebMethod
    @SneakyThrows
    public boolean isEmailExists(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "email") final String email
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().isEmailExists(email);
    }

    @WebMethod
    @SneakyThrows
    public @NotNull User createUser(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "login") final String login,
            @WebParam (name = "password") final String password
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password);
    }

    @WebMethod
    @SneakyThrows
    public @NotNull User createUserWithEmail(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "login") final String login,
            @WebParam (name = "password") final String password,
            @WebParam (name = "email") final String email
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, email);
    }

    @WebMethod
    @SneakyThrows
    public @NotNull User createUserWithRole(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "login") final String login,
            @WebParam (name = "password") final String password,
            @WebParam (name = "role") final Role role
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, role);
    }

    @WebMethod
    @SneakyThrows
    public @NotNull User createUserWithAll(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "login") final String login,
            @WebParam (name = "password") final String password,
            @WebParam (name = "email") final String email,
            @WebParam (name = "role") final String role
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, email, role);
    }

    @WebMethod
    @SneakyThrows
    public @NotNull User findUserByLogin(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findUserByLogin(login);
    }

    @WebMethod
    @SneakyThrows
    public @NotNull User findUserByEmail(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "email") final String email) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findUserByEmail(email);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable User removeUser(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "user") final User user) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeUser(user);
    }

    @WebMethod
    @SneakyThrows
    public @NotNull User removeUserByLogin(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "login") final String login) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeUserByLogin(login);
    }

    @WebMethod
    @SneakyThrows
    public @NotNull User removeUserByEmail(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "email") final String email) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeUserByEmail(email);
    }


    @WebMethod
    @SneakyThrows
    public @NotNull User lockUserByLogin(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().lockUserByLogin(login);
    }

    @WebMethod
    @SneakyThrows
    public @NotNull User unlockUserByLogin(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "login") final String login) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @WebMethod
    @SneakyThrows
    public @NotNull User lockUserByEmail(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "email") final String email
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().lockUserByEmail(email);
    }

    @WebMethod
    @SneakyThrows
    public @NotNull User unlockUserByEmail(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "email") final String email
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().unlockUserByEmail(email);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project findProjectByNameWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().findOneByName(name);
    }

    @WebMethod
    @SneakyThrows
    public boolean isProjectAbsentByNameWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().isAbsentByName(name);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public String getProjectIdByNameWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name ") final String name
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().getIdByName(name);
    }

    @WebMethod
    @SneakyThrows
    public void addProjectAllWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "collection") Collection<Project> collection
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getProjectService().addAll(collection);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project addProjectWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "project") final Project entity
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().add(entity);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public List<Project> findProjectAllWithoutUserId(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().findAll();
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project findProjectByIdWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "projectId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().findOneById(id);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Project findProjectByIndexWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().findOneByIndex(index);
    }

    @WebMethod
    @SneakyThrows
    public boolean isProjectAbsentByIdWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "projectId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().isAbsentById(id);
    }

    @WebMethod
    @SneakyThrows
    public boolean isProjectAbsentByIndexWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().isAbsentByIndex(index);
    }

    @WebMethod
    @SneakyThrows
    public String getProjectIdByIndexWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().getIdByIndex(index);
    }

    @WebMethod
    @SneakyThrows
    public int sizeProjectWithoutUserId(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().size();
    }

    @WebMethod
    @SneakyThrows
    public void clearProjectWithoutUserId(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getProjectService().clear();
    }

    @WebMethod
    @SneakyThrows
    public void removeProjectWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "project") final Project entity
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getProjectService().remove(entity);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public Project removeProjectByIdWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "projectId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().removeOneById(id);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public Project removeProjectByIndexWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getProjectService().removeOneByIndex(index);
    }


    @WebMethod
    @SneakyThrows
    public @Nullable Task findTaskByNameWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().findOneByName(name);
    }

    @WebMethod
    @SneakyThrows
    public boolean isTaskAbsentByNameWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().isAbsentByName(name);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public String getTaskIdByNameWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().getIdByName(name);
    }

    @WebMethod
    @SneakyThrows
    public void addTaskAllWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "collection") Collection<Task> collection
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getTaskService().addAll(collection);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task addTaskWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "task") final Task entity
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().add(entity);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable List<Task> findTaskAllWithoutUserId(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().findAll();
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task findTaskByIdWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "taskId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().findOneById(id);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task findTaskByIndexWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().findOneByIndex(index);
    }

    @WebMethod
    @SneakyThrows
    public boolean isTaskAbsentByIdWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "taskId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().isAbsentById(id);
    }

    @WebMethod
    @SneakyThrows
    public boolean isTaskAbsentByIndexWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().isAbsentByIndex(index);
    }

    @WebMethod
    @SneakyThrows
    public String getTaskIdByIndexWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().getIdByIndex(index);
    }

    @WebMethod
    @SneakyThrows
    public int sizeTaskWithoutUserId(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().size();
    }

    @WebMethod
    @SneakyThrows
    public void clearTaskWithoutUserId(
            @WebParam (name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getTaskService().clear();
    }

    @WebMethod
    @SneakyThrows
    public void removeTaskWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "task") final Task entity
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getTaskService().remove(entity);
    }

    @WebMethod
    @SneakyThrows
    public @Nullable Task removeTaskByIdWithoutUserId(
            @WebParam (name = "session") final Session session,
            @WebParam (name = "taskId") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getTaskService().removeOneById(id);
    }


}
