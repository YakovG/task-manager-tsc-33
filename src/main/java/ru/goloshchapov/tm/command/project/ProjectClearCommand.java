package ru.goloshchapov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.exception.entity.ProjectListIsEmptyException;
import ru.goloshchapov.tm.model.Project;

import java.util.List;

public final class ProjectClearCommand extends AbstractProjectCommand{

    @NotNull public static final String NAME = "project-clear";

    @NotNull public static final String DESCRIPTION = "Clear all projects";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT CLEAR]");
        @Nullable final List<Project> projects = serviceLocator.getProjectTaskService().findAllProjects(userId);
        if (projects == null) throw new ProjectListIsEmptyException();
        for (@NotNull final Project project: projects) {
            serviceLocator.getProjectTaskService().removeAllByProjectId(userId, project.getId());
        }
        projects.clear();
        serviceLocator.getProjectTaskService().clearProjects(userId);
    }
}
