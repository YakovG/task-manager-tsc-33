package ru.goloshchapov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;

public final class BackupClearCommand extends AbstractDataCommand{

    @NotNull
    public static final String NAME = "backup-clear";

    @NotNull public static final String DESCRIPTION = "Clear XML backup";

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return NAME;
    }

    @Override
    public @NotNull String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final File file = new File(BACKUP_XML);
        if (file.exists()) file.delete();
    }

}
